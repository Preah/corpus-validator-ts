import {IsString, IsInt, IsNotEmpty, IsArray, ArrayNotEmpty} from 'class-validator';

export default class Root {

    public errorList = [];

    @IsString() @IsNotEmpty()
    private id: string | null = null;

    @IsNotEmpty()
    private metadata : Metadata | null = null;
    
    @ArrayNotEmpty() @IsArray() @IsNotEmpty() 
    private document : Document[] | null = null;
}

export class Metadata {

    @IsString() @IsNotEmpty()
    private title: string | null = null;
    
    @IsString() @IsNotEmpty()
    private author: string | null = null;
    
    @IsString() @IsNotEmpty()
    private publisher: string | null = null;
    
    @IsString() @IsNotEmpty()
    private year: string | null = null;
    
    @ArrayNotEmpty() @IsArray() @IsNotEmpty()
    private annotation_level: string[] | null = null;
    
    @IsString()
    private note: string | null = null;    
}

export class Document {

    @IsString() @IsNotEmpty()
    private id: string | null = null;
    
    @IsNotEmpty()
    private metadata: DocumentMetadata | null = null;
    
    @ArrayNotEmpty() @IsArray() @IsNotEmpty() 
    private speaker: DocumentSpeaker[] | null = null;
    
    @ArrayNotEmpty() @IsArray() @IsNotEmpty() 
    private sentence: DocumentSentence[] | null = null;
    
    @ArrayNotEmpty() @IsArray()
    private CR: DocumentCR[] | null = null;

    @ArrayNotEmpty() @IsArray()
    private ZA: DocumentZA[] | null = null;
}

export class DocumentMetadata {

    @IsString() @IsNotEmpty()
    private title: string | null = null;
    
    @IsString() @IsNotEmpty()
    private author: string | null = null;
    
    @IsString() @IsNotEmpty()
    private publisher: string | null = null;

    @IsArray()
    private url: string[] | null = null;
    
    @IsString() @IsNotEmpty()
    private date: string | null = null;
    
    @IsString() @IsNotEmpty()
    private category: string | null = null;
    
    @IsArray()
    private note: string[] | null = null;
}

export class DocumentSpeaker {

    @IsInt() @IsNotEmpty()
    private id: number | null = null;
    
    @IsString() @IsNotEmpty()
    private age: string | null = null;
    
    @IsString() @IsNotEmpty()
    private home: string | null = null;
    
    @IsString() @IsNotEmpty()
    private job: string | null = null;
    
    @IsString() @IsNotEmpty()
    private sex: string | null = null;
}

export class DocumentSentence {

    @IsString() @IsNotEmpty()
    private id: string | null = null;

    @IsString() @IsNotEmpty()
    private form: string | null = null;
    
    @IsNotEmpty() 
    private utterance: DocumentSentenceUtterance | null = null;

    @ArrayNotEmpty() @IsArray() @IsNotEmpty() 
    private word: DocumentSentenceWord[] | null = null;
    
    @ArrayNotEmpty() @IsArray() @IsNotEmpty() 
    private morpheme: DocumentSentenceMorpheme[] | null = null;
    
    @ArrayNotEmpty() @IsArray() @IsNotEmpty() 
    private WSD: DocumentSentenceWSD[] | null = null;
    
    @ArrayNotEmpty() @IsArray() @IsNotEmpty() 
    private NE: DocumentSentenceNE[] | null = null;
    
    @IsArray()
    private DP: DocumentSentenceDP[] | null = null;

    @IsArray()
    private SRL: DocumentSentenceSRL[] | null = null;
}

export class DocumentSentenceUtterance {

    @IsString() @IsNotEmpty()
    private id: string | null = null;

    @IsString() @IsNotEmpty()
    private form: string | null = null;
    
    @IsString() @IsNotEmpty()
    private speaker_id: string | null = null;

    @IsString() @IsNotEmpty()
    private note: string | null = null;
}


export class DocumentSentenceWord {

    @IsInt() @IsNotEmpty()
    private id: number | null = null;
    
    @IsString() @IsNotEmpty()
    private form: string | null = null;
    
    @IsInt() @IsNotEmpty()
    private begin: number | null = null;
    
    @IsInt() @IsNotEmpty()
    private end: number | null = null;
}

export class DocumentSentenceMorpheme {

    @IsInt() @IsNotEmpty()
    private id: number | null = null;

    @IsString() @IsNotEmpty()
    private form: string | null = null;
    
    @IsString() @IsNotEmpty()
    private label: string | null = null;
    
    @IsInt() @IsNotEmpty()
    private word_id: number | null = null;

    @IsInt() @IsNotEmpty()
    private position: number | null = null;
}


export class DocumentSentenceWSD {
    
    @IsString() @IsNotEmpty()
    private word: string | null = null;
    
    @IsInt() @IsNotEmpty()
    private sense_id: number | null = null;
    
    @IsInt() @IsNotEmpty()
    private begin: number | null = null;
    
    @IsInt() @IsNotEmpty()
    private end: number | null = null;
}

export class DocumentSentenceNE {
    
    @IsInt() @IsNotEmpty()
    private id: number | null = null;

    @IsString() @IsNotEmpty()
    private form: string | null = null;

    @IsString() @IsNotEmpty()
    private label: string | null = null;
    
    @IsInt() @IsNotEmpty()
    private begin: number | null = null;
    
    @IsInt() @IsNotEmpty()
    private end: number | null = null;
}


export class DocumentSentenceDP {
    
    @IsInt() @IsNotEmpty()
    private word_id: number | null = null;

    @IsString() @IsNotEmpty()
    private word_form: string | null = null;
    
    @IsInt() @IsNotEmpty()
    private head: number | null = null;

    @IsString() @IsNotEmpty()
    private label: string | null = null;
    
    @ArrayNotEmpty() @IsArray() @IsNotEmpty()
    private dependent: number[] | null = null;
}

export class DocumentSentenceSRL {

    @IsNotEmpty()
    private predicate: DocumentSentenceSRLPredicate | null = null;
   
    @ArrayNotEmpty() @IsArray() @IsNotEmpty()
    private argument: DocumentSentenceSRLArgument[] | null = null;
}

export class DocumentSentenceSRLPredicate {

    @IsString() @IsNotEmpty()
    private form: string | null = null;

    @IsInt() @IsNotEmpty()
    private sense_id: number | null = null;

    @IsInt() @IsNotEmpty()
    private word_id: number | null = null;
}

export class DocumentSentenceSRLArgument {

    @IsInt() @IsNotEmpty()
    private word_id: number | null = null;

    @IsString() @IsNotEmpty()
    private word_form: string | null = null;

    @IsString() @IsNotEmpty()
    private label: string | null = null;
}

export class DocumentCR {

    @ArrayNotEmpty() @IsArray() @IsNotEmpty()
    private entity: DocumentCREntity[] | null = null;
}

export class DocumentCREntity {

    @ArrayNotEmpty() @IsArray() @IsNotEmpty()
    private mention: DocumentCREntityMention[] | null = null;
}

export class DocumentCREntityMention {

    @IsString() @IsNotEmpty()
    private form: string | null = null;

    @IsInt() @IsNotEmpty()
    private NE_id: number | null = null;

    @IsString() @IsNotEmpty()
    private sentence_id: string | null = null;

    @IsInt() @IsNotEmpty()
    private begin: number | null = null;

    @IsInt() @IsNotEmpty()
    private end: number | null = null;
}

export class DocumentZA {

    @IsString() @IsNotEmpty()
    private type: string | null = null;

    @IsNotEmpty()
    private predicate: DocumentZAPredicate | null = null;

    @IsNotEmpty()
    private antecedent: DocumentZAAntecedent | null = null;

}

export class DocumentZAPredicate {

    @IsString() @IsNotEmpty()
    private form: string | null = null;
    
    @IsInt() @IsNotEmpty()
    private word_id: number | null = null;

    @IsString() @IsNotEmpty()
    private sentence_id: string | null = null;
    
}

export class DocumentZAAntecedent {

    @IsString() @IsNotEmpty()
    private form: string | null = null;

    @IsString() @IsNotEmpty()
    private sentence_id: string | null = null;
    
    @IsInt() @IsNotEmpty()
    private begin: number | null = null;
    
    @IsInt() @IsNotEmpty()
    private end: number | null = null;
}