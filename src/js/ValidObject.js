
  let vaildObject = {
    id : 'Str [1]',
    metadata : {
        title : 'Str [1]',
        author : 'Str [1]',
        publisher : 'Str [1]',
        year : 'Str [1]',
        annotation_level : ['Str 1', 'Str N'],
        note : 'Str [0, 1]'
    },
    document : [{
        id : 'Str [1]',
        metadata : {
            title : 'Str [1]',
            author : 'Str [1]',
            publisher : 'Str [1]',
            url : ['Str 1', 'Str N'],
            date : 'Str [1]',
            category : 'Str [1]',
            note : ['Str 1', 'Str N']
        },
        speaker : [{
            id : 1,
            age : 'Str [1]',
            home : 'Str [1]',
            job : 'Str [1]',
            sex : 'Str [1]'
        }],
        sentence : [{
            id : 'Str [1]',
            form : 'Str [1]',
            utterance : {
                id : 'Str [1]',
                form : 'Str [1]',
                speaker_id : 'Str [1]',
                note : 'Str [1]'
            },
            word : [{
                id : 1,
                form : 'Str [1]',
                begin : 1,
                end : 1
            }],
            morpheme : [{
                id : 1,
                form : 'Str [1]',
                label : 'Str [1]',
                word_id : 1,
                position : 1
            }],
            WSD : [{
               word : 'Str [1]',
               sense_id : 1,
               begin : 1,
               end : 1 
            }],
            NE : [{
                id : 1,
                form : 'Str [1]',
                label : 'Str [1]',
                begin : 1,
                end : 1
            }],
            DP : [{
                word_id : 1,
                word_form : 'Str [1]',
                head : 1,
                label : 'Str [1]',
                dependent : [1, 2, 3]
            }],
            SRL : [{
                predicate : {
                    form : 'Str [1]',
                    sense_id : 1,
                    word_id : 1
                },
                argument : [{
                    word_id : 1,
                    word_form : 'Str [1]',
                    label : 'Str [1]'
                }]
            }]
        }],
        CR : [{
            entity : [{
                mention : [{
                    form : 'Str [1]',
                    NE_id : 1,
                    sentence_id : 'Str [1]',
                    begin : 1,
                    end : 1
                }]
            }]
        }],
        ZA : [{
            type : 'Str [1]',
                predicate : {
                    form : 'Str [1]',
                    word_id : 1,
                    sentence_id : 'Str [1]',
                },
                antecedent : {
                    form : 'Str [1]',
                    sentence_id : 'Str [1]',
                    word_id : 1,
                    begin : 1,
                    end : 1
                }
        }]
    }]
}

export default vaildObject