import Vue from 'vue';
import App from './App.vue';
import router from './router';
import vuetify from './plugins/vuetify';
// import TreeView from 'vue-json-tree-view';

Vue.config.productionTip = false;
// Vue.use(TreeView);

new Vue({
  router,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
