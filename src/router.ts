import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';

import Login from './components/Login.vue';
import Main from './components/Main.vue';
import NotFound from './components/NotFound.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes : [
    {path : '/', name : 'Login', component : Login},
    {path : '/main', name : 'Main', component : Main},
    {path : '*', name : 'NotFound', component : NotFound},
  ],
});
